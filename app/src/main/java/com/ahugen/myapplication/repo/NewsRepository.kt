package com.ahugen.myapplication.repo

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ahugen.myapplication.api.NewsService
import com.ahugen.myapplication.model.Article
import com.ahugen.myapplication.model.NewsResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewsRepository(private val service: NewsService) {

    fun getArticles() : LiveData<List<Article>> {
        val liveData: MutableLiveData<List<Article>> = MutableLiveData()
        service.getNewsResponse(NewsService.Helper.COUNTRY, NewsService.Helper.API_KEY)
            .enqueue(object : Callback<NewsResponse> {
                override fun onFailure(call: Call<NewsResponse>, t: Throwable) {
                    Log.e("Network error", t.toString())
                }

                override fun onResponse(
                    call: Call<NewsResponse>,
                    response: Response<NewsResponse>
                ) {
                    liveData.value = response.body()?.articles
                }
            })
        return liveData
    }

}