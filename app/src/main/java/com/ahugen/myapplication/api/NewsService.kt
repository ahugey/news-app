package com.ahugen.myapplication.api

import com.ahugen.myapplication.model.NewsResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsService {

    companion object Helper {
        const val BASE_URL = "https://newsapi.org/v2/"
        const val COUNTRY = "us"
        //having trouble grabbing BuildConfig
        const val API_KEY = "f71af7261c434b5d8be60816ed910d8b"


        fun getApiService(): NewsService {
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(NewsService::class.java)
        }
    }

    @GET("top-headlines")
    fun getNewsResponse(@Query ("country") country: String,
                    @Query("apiKey") apiKey: String ): Call<NewsResponse>
}