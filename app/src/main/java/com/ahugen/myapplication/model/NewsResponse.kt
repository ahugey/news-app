package com.ahugen.myapplication.model

data class NewsResponse(
    var status: String?,
    var totalResults: Long?,
    var articles: List<Article>?
)

data class Article (
    val source: Source,
    val author: String,
    val title: String,
    val description: String,
    val url: String,
    val urlToImage: String,
    val publishedAt: String,
    val content: String
)

data class Source (
    val id: String? = null,
    val name: String
)