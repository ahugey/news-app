package com.ahugen.myapplication.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.ahugen.myapplication.api.NewsService
import com.ahugen.myapplication.model.Article
import com.ahugen.myapplication.repo.NewsRepository

class NewsViewModel: ViewModel() {
    private val repo: NewsRepository = NewsRepository(NewsService.getApiService())

    fun getNewsArticles(): LiveData<List<Article>> {
        return repo.getArticles()
    }

}